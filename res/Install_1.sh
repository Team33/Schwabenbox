#!/bin/bash

# Server download
cd ~/../..
cd usr/local/bin/
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -;
cd

# Paketlisten aktualisieren und notwendige Pakete installieren
sudo apt-get update;
sudo apt-get updgrade;
sudo apt-get install gcc g++ build-essential -y nodejs sqlite3 libsqlite3-dev mailutils ssmtp zbar-tools python-zbar git-core;

# wiringPi
git clone git://git.drogon.net/wiringPi;
cd wiringPi
./build;

# Webserver installieren
cd ~/Schwabenbox/src/webserver
npm install;
CC=gcc-4.9 CXX=g++-4.9 npm install sqlite3;

return 0;
