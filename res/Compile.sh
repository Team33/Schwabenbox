#!/bin/bash
cd ../src/schwabenbox
g++ main.cpp src/database.cpp src/barcode.cpp src/email.cpp src/device.cpp src/easywsclient.cpp src/threads.cpp src/ISRs.cpp -lsqlite3 -lpthread -lwiringPi -std=c++11 -o schwabenbox
