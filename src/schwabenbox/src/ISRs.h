#ifndef ISRS_H
#define ISRS_H

#include <iostream>
#include <wiringPi.h>

#include "threads.h"
#include "device.h"
#include "barcode.h"
#include "database.h"
#include "email.h"
#include "enums.h"

using namespace std;



void ISR_start_scan(void);
void ISR_fail(void);

#endif // ISRS_H
