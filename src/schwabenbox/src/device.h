#ifndef DEVICE_H
#define DEVICE_H

#include <string>
#include <fstream>
#include <wiringPi.h>

#include "database.h"
#include "threads.h"
#include "easywsclient.hpp"
#include "ISRs.h"
#include "enums.h"

using namespace std;

//=========================these=are=eighty=characters=========================>

/**
  * @class device
  * @brief Class for handling hardware/prototype
  */

class device
{
public:

    /** @brief Constructor
     *
     */
    device ();


    /** @brief Destructor
     *
     */
    virtual ~device ();


    /** @brief Run device
     *
     * @return void
     */
    void vod_run();


    /** @brief Gets seconds since 01/01/1970
     *
     * @return int: seconds
     */
    int int_get_seconds_since_19700101();


    /** @brief Checks if system-time is between start-time and end-time of duty
     *
     * @return bool: true if device in duty, false if not
     */
    bool bol_device_in_duty();

//=========================these=are=eighty=characters=========================>

    /** @brief Gets state of start-switch, means checking representating file
     *  @brief for "0" or "1"
     *
     * @return bool: true if "1" in file, false if "0"
     */
    bool bol_get_state_of_start_switch();


    /** @brief Gets state of fail-switch, means checking representating file
     *  @brief for "0" or "1"
     *
     * @return bool: true if "1" in file, false if "0"
     */
    bool bol_get_state_of_fail_switch();


    /** @brief Prototype: Causes terminal output for simulating
     *
     * @param bool: pin-state after execute
     * @return void
     */
    void vod_set_door_opener( const bool& bol_state );

//=========================these=are=eighty=characters=========================>

    /** @brief Generates state of whole device for sending to website
     *
     * @return string: state coded in a row of zeros an ones
     */
    string str_generate_state_of_device();


    /** @brief Gets state of door-opener, means checking representating file
     *  @brief for "0" or "1"
     *
     * @return bool: true if "1" in file, false if "0"
     */
    bool bol_get_state_of_door_opener();

protected:

private:

};

#endif // DEVICE_H
