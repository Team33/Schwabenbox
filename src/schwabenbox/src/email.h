#ifndef EMAIL_H
#define EMAIL_H

#include <iostream>
#include <string>

#include "database.h"
#include "enums.h"

using namespace std;

//=========================these=are=eighty=characters=========================>

/**
  * @class email
  * @brief Handles mail messages and executes
  */

class email
{
public:

    /**
     * Empty Constructor
     */
    email ();

    /**
     * Empty Destructor
     */
    virtual ~email ();


    /** @brief Sends e-mail to all adresses in database
     *
     * @param char: describes why mail is sent
     *
     * @return void
     */
    void vod_send_fail_mail( const char& chr_kind_of_case );

//=========================these=are=eighty=characters=========================>

    /** @brief Sends e-mail to all adresses in database
     *
     * @param char: describes why mail is sent
     * @param string: scanned barcode to be insert into e-mail
     *
     * @return void
     */
    void vod_send_fail_mail( const string& str_current_barcode );


    /** @brief Sends e-mail to all adresses in database
     *
     * @param string: scanned barcode to be insert into e-mail
     *
     * @return void
     */
    void vod_send_delivery_mail( const string& str_current_barcode );

//=========================these=are=eighty=characters=========================>

protected:

private:

};

#endif // EMAIL_H
