#ifndef THREADS_H
#define THREADS_H

#include <iostream>

#include "device.h"
#include "database.h"
#include "email.h"
#include "enums.h"

using namespace std;


void *vod_open_door(void *i);
void *vod_scan(void *i);
void *vod_fail(void *i);
void new_thread(char what_for);


#endif // THREADS_H
