#include "device.h"


//=========================these=are=eighty=characters=========================>

device::device ()
{
    //! nothing to initialize
}

device::~device ()
{
}

void device::vod_run()
{
    //! initialisation easywsclient
    using easywsclient::WebSocket;
    WebSocket::pointer ws = WebSocket::from_url("ws://localhost:8080/foo");
    WebSocket::pointer wsp = &*ws;

    //! initialisation wiringPi and in-/outputs
    wiringPiSetup();
    pinMode( enums::ERROR_LED       , OUTPUT );
    pinMode( enums::DOOR_OPENER_LED , OUTPUT );
    pinMode( enums::IN_DUTY_LED     , OUTPUT );
    pinMode( enums::SCAN_BUTTON     , INPUT  );
    pinMode( enums::ERROR_BUTTON    , INPUT  );
    string str_give_gpio_rights = "sudo chmod -R 777 /sys/class/gpio";
    system( str_give_gpio_rights.c_str());

    //! set outputs to zero
    digitalWrite( enums::ERROR_LED      , 0 );
    digitalWrite( enums::DOOR_OPENER_LED, 0 );
    digitalWrite( enums::IN_DUTY_LED    , 0 );

    //! init interrupt service routines
    wiringPiISR( enums::SCAN_BUTTON , INT_EDGE_RISING   , &ISR_start_scan);
    wiringPiISR( enums::ERROR_BUTTON, INT_EDGE_BOTH     , &ISR_fail      );

    //! instances
    barcode* barcode;

    //! reset scan
    barcode->vod_scan_inactive();

    //! time defines
    int SLEEP_TIME_IN_LOOP = 10000;        // 10 000 milliseconds

//=========================these=are=eighty=characters=========================>

    //! loop forever
    while (1)
    {
        //! open door by webpage
        //! start new thread for door opener if message == open_door
        ws->poll();
        ws->dispatch([wsp](const string & message)
        {
            if ( message == "open_door" )
            {
                new_thread( enums::open_door );
            }
        } );

        //! is device in duty?
        if ( this->bol_device_in_duty() )
        {
            //! set signal light "in duty"
            digitalWrite( enums::IN_DUTY_LED, 1 );
        }
        else
        {
            //! reset signal light "in duty"
            digitalWrite( enums::IN_DUTY_LED, 0 );
        }

        //! send state to webserver
        ws->send( this->str_generate_state_of_device() );
        //! device sleep for 10 milliseconds to safe resources
        usleep( SLEEP_TIME_IN_LOOP );
    }

}

//=========================these=are=eighty=characters=========================>

int device::int_get_seconds_since_19700101()
{
    //! get and return system-time
    time_t tit_systemtime = time(&tit_systemtime);

    return tit_systemtime;
}


bool device::bol_device_in_duty()
{
    //! declare sqlite3 (-statement) instance
    string str_sqlstatement = "SELECT * FROM runtime";
    sqlite3* sql_database;
    sqlite3_stmt* sqs_statement;

    //! open database
    sqlite3_open("../database/schwabenbox.db", &sql_database);
    //! prepare statement
    sqlite3_prepare( sql_database, str_sqlstatement.c_str(),
                     -1, &sqs_statement, nullptr );
    //! get one row of database answer
    sqlite3_step( sqs_statement );
    //! get value from first row and first (0) column as start time
    int int_start_time = sqlite3_column_int( sqs_statement, 0 );
    //! get value from first row and second (0) column as end time
    int int_stop_time = sqlite3_column_int( sqs_statement, 1 );
    //! delete prepared statement
    sqlite3_finalize( sqs_statement );
    //! close database
    sqlite3_close( sql_database );

    //! get current system time
    // Source: https://www.c-plusplus.net/forum/25373-full
    time_t tit_systemtime;
    time(&tit_systemtime);
    struct tm stm_systemtime = *(localtime(&tit_systemtime));
    int int_system_hour = stm_systemtime.tm_hour;

    //! return true if current time between start/end time, false if not
    return ( ( int_system_hour >= int_start_time ) &&
             ( int_system_hour <  int_stop_time ));
}

//=========================these=are=eighty=characters=========================>

bool device::bol_get_state_of_start_switch()
{
    //! read input - start button
    return ( digitalRead( enums::SCAN_BUTTON ) == 1 );
}


bool device::bol_get_state_of_fail_switch()
{
    //! read input - error button
    return ( digitalRead( enums::ERROR_BUTTON ) == 1 );
}

//=========================these=are=eighty=characters=========================>

void device::vod_set_door_opener ( const bool& bol_state )
{
    //! set door opener to "bol_state"
    if ( bol_state )
    {
        digitalWrite( enums::DOOR_OPENER_LED, 1 );
    }
    else
    {
        digitalWrite( enums::DOOR_OPENER_LED, 0 );
    }
}

//=========================these=are=eighty=characters=========================>

string device::str_generate_state_of_device()
{
    /** protocol code example: 11001
      * 1: device active
      * 1: scanner active
      * 0: start switch not pushed
      * 0: fail switch not pushed
      * 1: door opener active
      */

    //! declare barcode instance and string for state
    barcode* barcode;
    string str_state = "";

    //! if device is active write "1"
    if ( this->bol_device_in_duty() )
    {
        str_state += "1";
    }
    //! else write "0"
    else
    {
        str_state += "0";
    }
    //! if scanner is active write "1" at the end of string
    if ( barcode->bol_is_scan_active() )
    {
        str_state += "1";
    }
    //! else write "0"
    else
    {
        str_state += "0";
    }
    //! if start switch is pushed write "1" at the end of string
    if ( this->bol_get_state_of_start_switch() )
    {
        str_state += "1";
    }
    //! else write "0"
    else
    {
        str_state += "0";
    }
    //! if fail switch is pushed write "1" at the end of string
    if ( this->bol_get_state_of_fail_switch() )
    {
        str_state += "1";
    }
    //! else write "0"
    else
    {
        str_state += "0";
    }
    //! if door opener is active write "1" at the end of string
    if ( this->bol_get_state_of_door_opener() )
    {
        str_state += "1";
    }
    //! else write "0"
    else
    {
        str_state += "0";
    }

    //! return coded string
    return str_state;
}

//=========================these=are=eighty=characters=========================>

bool device::bol_get_state_of_door_opener()
{
    return ( digitalRead( enums::DOOR_OPENER_LED ) == 1 );
}

//=========================these=are=eighty=characters=========================>
