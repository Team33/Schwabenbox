#ifndef ENUMS_H
#define ENUMS_H

using namespace std;


/** @class enums
  * @brief class to define nice names instead of numbers
  *
  *
  */

class enums
{

public:

    /** @brief to make pin-numbers readable
     *
     * @return
     *
     */

    enum PINS
    {
        ERROR_LED       = 0,
        DOOR_OPENER_LED = 2,
        IN_DUTY_LED     = 3,
        SCAN_BUTTON     = 4,
        ERROR_BUTTON    = 5
    };


    /** @brief only integers can be swiched
      *  needed at threads.cpp in switch-case
      * @return
      *
      */

    enum THREADS
    {
        open_door   = 1,
        scan        = 2,
        fail        = 3
    };

    enum MESSAGE
    {
        no_barcode          = 1,
        delivery_impossible = 2,
        delivered           = 3,
        unknown_barcode     = 4
    };

};


#endif // ENUMS_H
