////////////////////////////////// DEFINITION //////////////////////////////////
var express = require('express'); // require express
var path = require('path');
var sqlite3 = require('sqlite3').verbose(); //sqlite3
var validator = require('validator'); //validates user inputs

var router = express.Router(); // create router object
var db = new sqlite3.Database('../database/schwabenbox.db'); //Database path

var startpageroutes = require('./startpageroutes')(router, db);

var packagesroutes = require('./packagesroutes')(router, db, validator);

var systemlogroutes = require('./systemlogroutes')(router, db);

var packagesroutes = require('./settingsroutes')(router, db, validator);

// export router
module.exports = router;
