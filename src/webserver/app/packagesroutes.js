module.exports = function(router, db, validator){
//////////////////////////////PACKAGES PAGE ////////////////////////////////////
/**
 * creates a time stamp that is easy to sort
 * @return time_stamp {string} the current time stamp
 */
function create_time_stamp() {
    //write time parts in vars
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    //convert from single digit to binary
    month = ((month < 10) ? "0" + month : month);
    day = ((day < 10) ? "0" + day : day);
    hours = ((hours < 10) ? "0" + hours : hours);
    minutes = ((minutes < 10) ? "0" + minutes : minutes);
    seconds = ((seconds < 10) ? "0" + seconds : seconds);
    //build a string out of the parts
    var time_stamp = year + "/" + month + "/" + day + " " +
        hours + ":" + minutes + ":" + seconds;
    return time_stamp;
}

/**
 * Do the databasequery for the packages page and render the page
 * @param {object} res the response object
 * @param {string} stmt the colums whereby should be sorted
 */
function select_from_packages(stmt_expected_tab, stmt_delivered_tab, res) {
    //if the string is empty do the default databasequery
    if (stmt_expected_tab == '') {
        stmt_expected_tab = "SELECT * FROM Pakete WHERE delivery_time_stamp=''" +
            " ORDER BY entry_time_stamp DESC";
    } else {
        stmt_expected_tab = "SELECT * FROM Pakete WHERE delivery_time_stamp=''" +
            " ORDER BY" + stmt_expected_tab;
    }
    //if the string is empty do the default databasequery
    if (stmt_delivered_tab == '') {
        stmt_delivered_tab = "SELECT * FROM Pakete WHERE delivery_time_stamp" +
            " != '' ORDER BY delivery_time_stamp DESC";
    } else {
        stmt_delivered_tab = "SELECT * FROM Pakete WHERE delivery_time_stamp" +
            " != '' ORDER BY" + stmt_delivered_tab;
    }
    var expecteddata = []; //init arraay for database results expexted packages
    var delivereddata = []; //init array for database results delivered packages
    db.serialize(function() {
        //do the databasequerys and push the result at the end of arrays
        db.each(stmt_expected_tab, function(err, row) {
            expecteddata.push(row);
        });
        db.each(stmt_delivered_tab, function(err, row) {
            delivereddata.push(row);
        }, function() {
            //render page with datas
            res.render('pages/packages', {
                expecteddata: expecteddata,
                delivereddata: delivereddata
            });
        });
    });
}

//routes for packages page
router.get('/packages', function(req, res) {
    // init the statement strings with no value for default databasequery
    var statement_for_expected = "";
    var statement_for_delivered = "";
    //do the default databasequerys and render the page
    select_from_packages(statement_for_expected, statement_for_delivered, res);
});

router.post('/packages', function(req, res) {
    // init the statement strings with no value for default databasequery
    var statement_for_expected = "";
    var statement_for_delivered = "";
    //check if button add package was used and if there is somthing in the var
    if ((req.body.add == 1) &&
        (req.body.addtrackingnumber != "") &&
        (validator.isAlphanumeric(req.body.addtrackingnumber, ['de-DE']))) {
        //console logs for debugging
        //console.log('Sendungsnummer: ' + req.body.addtrackingnumber);
        //console.log('Paketname: ' + req.body.packagename);
        db.serialize(function() {
          var tryoutpackagename = req.body.packagename;
          var tryoutpackagename= tryoutpackagename.replace(/ /g ,"");

            if((req.body.packagename != "") &&
            (validator.isAlphanumeric(tryoutpackagename, ['de-DE']))){
              var stmt = db.prepare("INSERT OR IGNORE" +
                  " INTO Pakete VALUES (?,?,?,?)");
              stmt.run(req.body.addtrackingnumber,
                  create_time_stamp(),
                  '',
                  req.body.packagename);
            }

            else if (req.body.packagename == "") {
            var stmt = db.prepare("INSERT OR IGNORE" +
                " INTO Pakete VALUES (?,?,?,?)");
            stmt.run(req.body.addtrackingnumber,
                create_time_stamp(),
                '',
                '');
            stmt.finalize;
            }
            select_from_packages(statement_for_expected,
                statement_for_delivered,
                res);
        });

        //check if button change packagenumber was used and var isn't empty
    } else if ((req.body.change == 1) &&
               (req.body.oldtrackingnumber != "") &&
               (validator.isAlphanumeric(req.body.oldtrackingnumber, ['de-DE']))) {
        //console logs for debugging
        //console.log('SN ändern: ' + req.body.oldtrackingnumber);
        //console.log('SN neu: ' + req.body.newtrackingnumber);
        //console.log('Paketname ändern: ' + req.body.newpackagename);

        db.serialize(function() {
          var tryoutnewpackagename = req.body.newpackagename;
          var tryoutnewpackagename= tryoutnewpackagename.replace(/ /g ,"");

            if ((req.body.newpackagename != "") &&
                (validator.isAlphanumeric(tryoutnewpackagename, ['de-DE'])) &&
                (req.body.newtrackingnumber != "") &&
                (validator.isAlphanumeric(req.body.newtrackingnumber, ['de-DE']))) {
                var stmt = db.prepare("UPDATE Pakete SET package_nr=(?)," +
                    " package_name=(?) WHERE package_nr=(?)");
                stmt.run(req.body.newtrackingnumber,
                    req.body.newpackagename,
                    req.body.oldtrackingnumber);
                stmt.finalize;

            } else if ((req.body.newpackagename != "") &&
                       (validator.isAlphanumeric(req.body.newpackagename, ['de-DE']))) {
                var stmt = db.prepare("UPDATE Pakete SET package_name = (?)" +
                    " WHERE package_nr = (?)");
                stmt.run(req.body.newpackagename, req.body.oldtrackingnumber);
                stmt.finalize;

            } else if ((req.body.newtrackingnumber != "") &&
                       (validator.isAlphanumeric(req.body.newtrackingnumber, ['de-DE']))){
                var stmt = db.prepare("UPDATE Pakete SET package_nr = (?)" +
                    " WHERE package_nr = (?)");
                stmt.run(req.body.newtrackingnumber,
                    req.body.oldtrackingnumber);
                stmt.finalize;
            }

            select_from_packages(statement_for_expected,
                statement_for_delivered,
                res);
        });
        //check if button remove package was used and something is in the var
    } else if ((req.body.remove == 1) &&
        (req.body.removetrackingnumber != "") &&
        (validator.isAlphanumeric(req.body.removetrackingnumber, ['de-DE']))) {
        //console log for debugging
        //console.log('SN entfernen: '+ req.body.removetrackingnumber);

        db.serialize(function() {
            var stmt = db.prepare("DELETE FROM Pakete WHERE package_nr = (?)");
            stmt.run(req.body.removetrackingnumber);
            stmt.finalize;
            select_from_packages(statement_for_expected,
                statement_for_delivered,
                res);
        });
        //check if the user wants to sort the database by trackingnumber
    } else if (req.body.expectedtrackingnumber) {
        statement_for_expected = " LOWER(package_nr) , package_nr";
        select_from_packages(statement_for_expected,
            statement_for_delivered,
            res);
        //check if the user wants to sort the database by packagename
    } else if (req.body.expectedpackagename) {
        statement_for_expected = " LOWER(package_name) ,package_name";
        select_from_packages(statement_for_expected,
            statement_for_delivered,
            res);
        ///sort the delivered package table by trackingnumber
    } else if (req.body.deliveredtrackingnumber) {
        statement_for_delivered = " LOWER(package_nr), package_nr";
        select_from_packages(statement_for_expected,
            statement_for_delivered,
            res);
        // sort the delivered package table by packagename
    } else if (req.body.deliveredpackagename) {
        statement_for_delivered = " LOWER(package_name), package_name";
        select_from_packages(statement_for_expected,
            statement_for_delivered,
            res);
        //sort the delivered package table by entry time stamp
    } else if (req.body.deliveredentrydate) {
        statement_for_delivered = " entry_time_stamp DESC";
        select_from_packages(statement_for_expected,
            statement_for_delivered,
            res);
        //default case (sort by entry time stamp (expected packages table)
        //and delivery time (delivered packages table) )
    } else {
        select_from_packages(statement_for_expected,
            statement_for_delivered,
            res);
    }
});
}
